package stepdefinations;


import core.TestBase;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;

public class StepDef extends TestBase {


    @Given("^a user open the chrome and navigate to https://www\\.amazon\\.com$")
    public void a_user_open_the_chrome_and_navigate_to_https_www_amazon_com() {
        initializeDriver();
        driver.get("https://www.amazon.com");
    }

    @When("^a user can see the amazon homepage$")
    public void a_user_can_see_the_amazon_homepage() {
        homePage.validateHomePageIsDisplayed();
    }

    @Then("^user clicks on the sign in button$")
    public void user_clicks_on_the_sign_in_button() {
        homePage.clickOnSignIn();
    }

    @Then("^user sends (.*) in the phone field$")
    public void user_sends_phone_number_in_the_required_field(String phone) {
        signInPage.typePhoneNumber(phone);
    }

    @Then("^user clicks on continue button$")
    public void user_clicks_on_continue_button() {
        driver.findElement(By.id("continue")).click();

    }

    @Then("^user sends (.*) in the password field$")
    public void user_sends_password_in_the_required_field(String pass) {
        driver.findElement(By.id("ap_password")).sendKeys(pass);

    }

    @Then("^user clicks on sign in$")
    public void user_clicks_on_sign_in() {
        driver.findElement(By.id("signInSubmit")).click();
    }

    @Then("^user can see amazon home page as a logged in user$")
    public void user_can_see_amazon_home_page_as_a_logged_in_user() {
        System.out.println("User logged in");
    }


    @Then("^user clicks on the cart button$")
    public void user_clicks_on_the_cart_button() {
        System.out.println(":cart button clicked");

    }

    @Then("^user validates cart page is displayed$")
    public void user_validates_cart_page_is_displayed() {
        System.out.println("cart page displayed");

    }

    @And("^user close the browser$")
    public void userCloseTheBrowser() {
        driver.quit();
    }
}
