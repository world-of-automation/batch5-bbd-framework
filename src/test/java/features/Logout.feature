Feature: user being able to login

  Scenario: user with correct credentials can login successfully
    Given a user open the chrome and navigate to https://www.amazon.com
    When a user can see the amazon homepage
    Then user clicks on the sign in button
    Then user sends phone number in the required field
    Then user clicks on continue button
    Then user sends password in the required field
    Then user clicks on sign in
    Then user can see amazon home page as a logged in user
    And user close the browser


  #@Regression
  Scenario: user can go to the cart page without loggin in
    Given a user open the chrome and navigate to https://www.amazon.com
    When a user can see the amazon homepage
    Then  user clicks on the cart button
    Then user validates cart page is displayed
    And user close the browser