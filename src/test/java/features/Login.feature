Feature: user being able to login

  @Regression
  Scenario Outline: user with correct credentials can login successfully
    Given a user open the chrome and navigate to https://www.amazon.com
    When a user can see the amazon homepage
    Then user clicks on the sign in button
    Then user sends <phone_number> in the phone field
    Then user clicks on continue button
    Then user sends <password> in the password field
    Then user clicks on sign in
    Then user can see amazon home page as a logged in user
    And user close the browser

    Examples:
      | phone_number | password |
      | 9293739212   | 12312312 |
      | 3478631519   | 12312312 |


  Scenario: user can go to the cart page without loggin in
    Given a user open the chrome and navigate to https://www.amazon.com
    When a user can see the amazon homepage
    Then  user clicks on the cart button
    Then user validates cart page is displayed
    And user close the browser