package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage {
    private static final Logger LOGGER = Logger.getLogger(SignInPage.class);


    @FindBy(id = "ap_email")
    private WebElement emailField;

    public void typePhoneNumber(String value) {
        emailField.sendKeys(value);
        LOGGER.info("Phone numberwas sent in the required field");
    }

}
