package pages;

import core.TestBase;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage {

    private static final Logger LOGGER = Logger.getLogger(HomePage.class);

    @FindBy(id = "nav-link-accountList-nav-line-1")
    private WebElement signInBtn;

    public void validateHomePageIsDisplayed() {
        Assert.assertTrue(signInBtn.isDisplayed());
        LOGGER.info("Account List is displayed");

        String currentUrl = TestBase.driver.getCurrentUrl();
        Assert.assertEquals(currentUrl, "https://www.amazon.com/");
        LOGGER.info(currentUrl + " is validated");
    }

    public void clickOnSignIn() {
        signInBtn.click();
    }
}
